app.controller("CatController", ["$scope", function($scope) {

	var makeOptions = function(maxAge) {
		var options = [];
		var currentYear = new Date().getFullYear();
		for (var i = 0; i <= maxAge; i++) {
			options.push({
				value: i,
				year: currentYear - i
			});
		}
		return options;
	};

	$scope.options = makeOptions(50);

	$scope.cats = [];

	$scope.submit = function(name, age) {
		$scope.cats.push({
			name: name,
			age: age
		});
	};

	$scope.form = true;
	$scope.catsList = false;
	$scope.activeForm = "active";
	$scope.activeCats = "";

	$scope.showForm = function() {
		$scope.form = true;
		$scope.catsList = false;
		$scope.activeForm = "active";
		$scope.activeCats = "";
	};

	$scope.showCats = function() {
		$scope.form = false;
		$scope.catsList = true;
		$scope.activeForm = "";
		$scope.activeCats = "active";
	};

}]);