app.controller("LabsController", ["$scope", function($scope) {

	$scope.labs = [
		{
			name: "Kasutajaliidese prototüüpimine",
			due: "28.04.2016 00:00",
			status: "warning",
			new: false
		},
		{
			name: "AJAX kasutajaliidese viimistlemine",
			due: "28.04.2016 00:00",
			status: "neutral",
			new: true
		}
	];

	$scope.newLab = function() {
		console.log("Make new lab.");
	}

}]);