app.controller("MaterialsController", ["$scope", function($scope, $location) {

	$scope.categories = [
		{
			category: "Lugemist",
			materials: [
				{
					name: "A List Apart: In Defence of Eye Candy",
					link: "http://alistapart.com/article/indefenseofeyecandy",
					description: "Miks ilus liides on \"parem\"."
				},
				{
					name: "Spolsky: User Interface Design For Programmers",
					link: "http://www.joelonsoftware.com/uibook/fog0000000249.html",
					description: "Kontrollitundest, kasutajamudelist ja päris inimestest."
				},
				{
					name: "Brandon Walkin: Managing UI complexity",
					link: "http://www.brandonwalkin.com/blog/2009/08/10/managing-ui-complexity/",
					description: "Sellest kuidas asju kasutaja eest ära peita."
				},
				{
					name: "Ask Tog: First Principles of User Interaction Design",
					link: "http://www.asktog.com/basics/firstPrinciples.html",
					description: "Tee liides valmis ja loe see uuesti läbi."
				},
				{
					name: "Nielsen: 10 heuristilist kriteeriumit",
					link: "http://www.nngroup.com/articles/ten-usability-heuristics/",
					description: "Liidese testimiseks."
				},
				{
					name: "UI-Patterns.com",
					link: "http://ui-patterns.com/",
					description: "Parempoolne menüü."
				},
				{
					name: "Spolsky: The Absolute Minimum Every Software Developer Absolutely, Positively Must Know About Unicode and Character Sets (No Excuses!)",
					link: "http://www.joelonsoftware.com/articles/Unicode.html"
				},
				{
					name: "Kuidas vormid koledaks lähevad",
					link: "http://friedcellcollective.net/outbreak/2007/12/13/messing-up-the-interface/"
				},
				{
					name: "Vormide Disainist",
					link: "http://www.lukew.com/ff/entry.asp?1502"
				}
			]
		},
		{
			category: "Tehniline",
			materials: [
				{
					name: "CSS",
					link: "http://www.w3schools.com/css/default.asp",
					description: "Peab aru saama põhimõtetest ja näidetest. Konkreetsete omaduste ja atribuudite nimesid ise välja ei pea mõtlema."
				},
				{
					name: "Javascript",
					link: "http://www.w3schools.com/js/default.asp",
					description: "Peab aru saama väikestest näidetest a la js inclusion, vormist teksti kättesaamine ja muutmine, domi kättesaamine ja muutmine. Ei pea oskama erinevaid teeke."
				},
				{
					name: "Json/ajax",
					link: "http://www.w3schools.com/ajax/default.asp",
					description: "Peab oskama kirjutada väikese JSONi kasutava ajaxi näite. Väike progeülesanne."
				},
				{
					name: "Cookied ja sessioonid",
					link: "http://www.quirksmode.org/js/cookies.html",
					description: "Peab aru saama cookie põhimõtetest server-side ja aru saama javascripti näidetest cookiede kasutamise kohta. Programmeerimisülesannet selle kohta ei tule."
				},
				{
					name: "Cgi protokoll",
					link: "http://www.jmarshall.com/easy/cgi/",
					description: "Peab aru saama, kuidas vormisisusid postitaktakse, mis vahe on GET ja POST protokollil."
				}
			]
		}
	];

	$scope.navigateTo = function(link) {
		window.location.assign(link);
	};

}]);