app.controller("LandingPageController", ["$scope", function($scope) {

	$scope.courses = [
		{
			name: "Kasutajaliidesed",
			image: "img/education-header.jpg",
			progress: "Progress Bar"
		},
		{
			name: "Ajalugu",
			image: "img/study-abroad.jpg.pagespeed.ce.bRzRYdvaPx.jpg",
			progress: "Progress Bar"
		},
		{
			name: "Veebirakenduste arhidektuur",
			image: "img/ec5ac431.jpg",
			progress: "Progress Bar"
		},
		{
			name: "Makro- ja mikroökonoomika",
			image: "img/fian	ls.jpg",
			progress: "Progress Bar"
		},
		{
			name: "Tõenäosusteooria ja matemaatiline statistika",
			image: "img/Studying-book.jpg",
			progress: "Progress Bar"
		}
	];

}]);