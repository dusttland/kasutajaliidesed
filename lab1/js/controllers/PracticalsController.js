app.controller("PracticalsController", ["$scope", function($scope) {

	$scope.practicals = [
		{
			name: "Prototüüpimine paberil",
			due: "28.04.2016 00:00",
			status: "positive",
			new: false
		},
		{
			name: "Veebilehtede põhjad",
			due: "28.04.2016 00:00",
			status: "positive",
			new: true
		},
		{
			name: "Frustratsiooniliides",
			due: "28.04.2016 00:00",
			status: "negative",
			new: true
		},
		{
			name: "Kassid",
			due: "28.04.2016 00:00",
			status: "neutral",
			new: false
		},
		{
			name: "Nurgelised kassid",
			due: "28.04.2016 00:00",
			status: "warning",
			new: true
		},
		{
			name: "Süntaktiliselt äge CSS",
			due: "28.04.2016 00:00",
			status: "neutral",
			new: true
		}
	];

}]);