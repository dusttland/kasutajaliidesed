app.controller("ProgressController", ["$scope", function($scope) {

	const STATUS_GOOD = 1;
	const STATUS_OK = 2;
	const STATUS_BAD = 3;
	const STATUS_COMPLETE = 4;

	var getStatus = function(status) {
		switch (status) {

			case STATUS_GOOD:
				return {
					text: "Hea töö! Sa püsid järjel!",
					progressBarStyle: "progress-bar-success",
					showAdditionalInfo: true
				};

			case STATUS_OK:
				return {
					text: "Tähtaeg on lähenemas.",
					progressBarStyle: "progress-bar-warning",
					showAdditionalInfo: true					
				};

			case STATUS_BAD:
				return {
					text: "Mõni tähtaeg on ületatud!",
					progressBarStyle: "progress-bar-danger",
					showAdditionalInfo: true
				};

			case STATUS_COMPLETE:
				return {
					text: "Palju õnne! Kursus on läbitud!",
					progressBarStyle: "progress-bar-success",
					showAdditionalInfo: false
				};

		}
	};

	$scope.progress = {
		percentage: 70,
		status: getStatus(1)
	};

}]);