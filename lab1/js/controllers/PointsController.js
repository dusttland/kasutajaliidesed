app.controller("PointsController", ["$scope", function($scope) {

	$scope.pointSubjects = [
		{
			name: "Prototüüpimine paberil",
			required: false,
			graded: true,
			points: 1,
			maxPoints: 1,
			minPoints: 0
		},
		{
			name: "Veebilehtede põhjad",
			required: false,
			graded: true,
			points: 1,
			maxPoints: 1,
			minPoints: 0
		},
		{
			name: "Frustratsiooniliides",
			required: false,
			graded: true,
			points: 0,
			maxPoints: 1,
			minPoints: 0
		},
		{
			name: "Kassid",
			required: false,
			graded: true,
			points: 1,
			maxPoints: 1,
			minPoints: 0
		},
		{
			name: "Nurgelised kassid",
			required: false,
			graded: true,
			points: 1,
			maxPoints: 1,
			minPoints: 0
		},
		{
			name: "Süntaktiliselt äge CSS",
			required: false,
			graded: true,
			points: 1,
			maxPoints: 1,
			minPoints: 0
		},
		{
			name: "Kasutajaliidese prototüüpimine",
			required: true,
			graded: true,
			points: 28,
			maxPoints: 30,
			minPoints: 15
		},
		{
			name: "AJAX kasutajaliidese viimistlemine",
			required: true,
			graded: true,
			points: 10,
			maxPoints: 30,
			minPoints: 15
		},
		{
			name: "Kontrolltöö",
			required: true,
			graded: false,
			points: 0,
			maxPoints: 40,
			minPoints: 20
		}
	];

	function getSumFromPointSubjects(pointSubjects) {
		var minPoints = 0,
		    maxPoints = 0,
		    points = 0;

		for (var i = pointSubjects.length - 1; i >= 0; i--) {
			minPoints += pointSubjects[i].minPoints;
			maxPoints += pointSubjects[i].maxPoints;
			points += pointSubjects[i].points;
		}

		return {
			minPoints: minPoints,
			maxPoints: maxPoints,
			points: points
		};
	}

	$scope.sum = getSumFromPointSubjects($scope.pointSubjects);

}]);