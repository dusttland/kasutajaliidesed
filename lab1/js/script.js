
$(document).ready(function() {

	$(window).scroll(function() {
		decideNavbarTitleHiding();
	});

	function decideNavbarTitleHiding() {
		var heightPoint = getNavbarTitleHideHeightPoint();
		var scrollHeight = $(window).scrollTop();

		if (scrollHeight < heightPoint) {
			hideNavbarTitle();
		} else {
			showNavbarTitle();
		}
	}

	function getNavbarTitleHideHeightPoint() {
		var headerTop = $("header").offset().top;
		var headerHeight = $("header").height() - $("#navbar").height();
		return (headerTop + headerHeight) / 2;
	}

	function showNavbarTitle() {
		$("#navbar-title").fadeIn(300);
	}

	function hideNavbarTitle() {
		$("#navbar-title").fadeOut(150);
	}
});