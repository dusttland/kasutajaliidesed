/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
const SCROLLSPY_OFFSET = 100;
const NAVBAR_HEIGHT = 50;

$(function() {
    $("a.page-scroll").bind("click", function(event) {
        var $anchor = $(this);

        var windowHeight = $(window).height();
        var pageHeight = $("body").height();

        var sectionOffsetTop = $($anchor.attr("href")).offset().top
        var maxOffsetTop = pageHeight - windowHeight;

        var offsetTop = sectionOffsetTop - NAVBAR_HEIGHT;
        if (offsetTop < 0)
            offsetTop = 0;
        if (offsetTop > maxOffsetTop)
        	offsetTop = maxOffsetTop;
        
        $("html, body").stop().animate({
            scrollTop: offsetTop
        }, 1500, "easeInOutExpo");

        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$("body").scrollspy({
    target: ".navbar-fixed-top",
    offset: NAVBAR_HEIGHT + SCROLLSPY_OFFSET
})

// Closes the Responsive Menu on Menu Item Click
$(".navbar-collapse ul li a").click(function() {
    $(".navbar-toggle:visible").click();
});