app.directive("contactInfo", function() {
	return {
		restrict: "E",
		scope: {
			contact: "="
		},
		templateUrl: "js/directives/contactInfo.html"
	};
});

app.directive("courseThumbnail", function() {
	return {
		restrict: "E",
		scope: {
			course: "="
		},
		templateUrl: "js/directives/courseThumbnail.html"
	};
});

app.directive("secondaryThumbnail", function() {
	return {
		restrict: "E",
		scope: {
			subject: "=",
			index: "@"
		},
		templateUrl: "js/directives/secondaryThumbnail.html"
	};
});

app.directive("secondaryThumbnailAdd", function() {
	return {
		restrict: "E",
		scope: {
			navigateFunction: "&"
		},
		templateUrl: "js/directives/secondaryThumbnailAdd.html"
	};
});

app.directive("materialCategory", function() {
	return {
		restrict: "E",
		scope: {
			category: "="
		},
		templateUrl: "js/directives/materialCategory.html"
	};
});

app.directive("pointTable", function() {
	return {
		restrict: "E",
		scope: {
			pointSubjects: "=",
			sum: "="
		},
		templateUrl: "js/directives/pointTable.html"
	};
});

app.directive("primaryThumbnail", function() {
	return {
		restrict: "E",
		scope: {
			subject: "=",
			index: "@"
		},
		templateUrl: "js/directives/primaryThumbnail.html"
	};
});

app.directive("primaryThumbnailAdd", function() {
	return {
		restrict: "E",
		scope: {
			navigateFunction: "&"
		},
		templateUrl: "js/directives/primaryThumbnailAdd.html"
	};
});

app.directive("progressBar", function() {
	return {
		restrict: "E",
		scope: {
			progress: "="
		},
		templateUrl: "js/directives/progressBar.html"
	};
});