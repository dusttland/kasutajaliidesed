app.directive("courseThumbnail", function() {
	return {
		restrict: "E",
		scope: {
			course: "="
		},
		templateUrl: "js/directives/courseThumbnail.html"
	};
});